# SmartVase #

SmartVase is a plant container that follows the IoT approch. It provides several services to the final user, in order to support him/her during the plant care process. 

This repository contains an idea (still ongoing) for the *Make-your-Thing* exercise, developed during an university course (*Pervasive Computing*).

## Features ##

- Useful data getering
	- humidity sensor, to check the soil status
	- ph sensor, in order to detect acidic soil 
	- temperature sensor, useful to analize the environment temperature
	- lightness sensor, to check whether the plant is receiving enough light
- Control the water needs of the plant
	- a sensor detects the soil's humidity level 
	- an actuator permits the automatic release of water in the soil
- Handle plant nutrition 
	- an actuator permits the automatic release of nutrients in the soil
	
## Development ##

SmartVase is meant to be a thing adopting the *Web of Things* philosophy. Hence, it has to define a set of APIs that enable a uniform access to it across the web.
In order to satisfy this requirement, a first sample of APIs has been developed, following a *RESTful* approch. 

These APIs could be found [here](https://app.swaggerhub.com/apis/gmp/smartvase/1.0.0).

## Further (unimplemented) features ##

- Automate the plant care process
	- self watering 
	- self nutrition
- Monitoring plant growth through one or more proximity sensors